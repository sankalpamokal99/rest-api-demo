const express = require('express');
const routes = require('./routes/api');
const mongoose = require("mongoose");
// set up express app

const app = express();

// connect to mongodb
mongoose.connect("mongodb://localhost/restapidb", { useNewUrlParser: true }).then(() => {
    console.log("Connected to Database");
}).catch((err) => {
    console.log("Not Connected to Database ERROR! ", err);
});
mongoose.Promise = global.Promise;

//  middleware

app.use(express.json());

//  initialized routes

app.use("/api", routes);

//  error handling middleware


// listen for requests

app.listen(process.env.PORT || 4000, () => {
    console.log("Listening for requests");
});

