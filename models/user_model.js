const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// create schema and model

const UserSchema = Schema({
    name: {
        type: String,
        required: [true, "Name is required"]
    },
    rank: {
        type: String,
    },
    available: {
        type: Boolean,
        default: false
    },
    available2: {
        type: Boolean,
        default: false
    }
});

const User = mongoose.model('user', UserSchema);

module.exports = User;