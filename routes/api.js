const express = require('express');
const User = require('../models/user_model');
const router = express.Router();


//get list
router.get("/ninjas", (req, res, next) => {
    User.find({}).then((user) => {
        res.send(user);
    });
});

// add to list
router.post("/ninjas", (req, res, next) => {
    const user = new User(req.body);
    user.save().then(() => {
        res.status(200).send({
            user
        });
    }).catch((e) => {
        res.status(400).send({
            message: e.message
        });
    });
});

//update from list
router.put("/ninjas/:id", (req, res, next) => {
    User.findByIdAndUpdate({ _id: req.params.id }, req.body).then(() => {
        User.findOne({ _id: req.params.id }).then((user) => {
            res.send(user);
        }).catch((e) => {
            res.status(400).send({
                message: "${e.message} server issue"
            });
        });
    }).catch((e) => {
        res.status(404).send({
            message: "id not found"
        });
    });
});

//delete from list
router.delete("/ninjas/:id", (req, res, next) => {

    User.findById({ _id: req.params.id }).then((user) => {
        User.findByIdAndRemove({ _id: req.params.id }).then(() => {
            res.status(200).send({ message: "user deleted" })
        }).catch((e) => {
            res.status(400).send({ message: "server issue" });
        });
    }).catch((e) => {
        res.status(404).send({ message: "not found" });
    });
});

module.exports = router;